const fs = require ('fs')



//datos 
let fecha =  new Date();

//dia de la semana
let diaSemana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
let diaSemana1 = diaSemana[fecha.getDay()]

//HOra
let hora = new Date()
horas = hora.toLocaleTimeString()

//mes
let monthYear = ['Enero', 'Febrero', 'Marzo' , 'Abril' , 'Mayo ', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre','Noviembre', 'Diciembre']
let monthYear1 = monthYear[fecha.getMonth()]

//date_2
date_2 = ` ${fecha.getDate()} / ${fecha.getMonth()+1} / ${fecha.getFullYear()}`

//controla  el formato de AM o PM
let horas1 = hora.getHours();
let ampm = horas1 >= 12 ? 'PM' : 'AM';


class Hours {
  constructor (){
const DataHora = 
  {
    Day: diaSemana1,
    Date_1: `${fecha.getDate()} de ${monthYear1} `,
    Year:fecha.getFullYear(),
    Date_2:date_2,
    hour:horas,
    Prefix:ampm
  }
    this.DataHora = DataHora

  }
}

module.exports = Hours